import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Arrays.asList;

public class SocketService {
    private static Socket socket;
    public static void connectToRobot() {
        try{
            socket = new Socket(InetAddress.getByName("172.23.5.203"),29999);
            BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
            InputStreamReader isr = new InputStreamReader(bis, "US-ASCII");
            int c;
            while((c = isr.read()) != 10){
                System.out.print((char)c);
            }
            System.out.println();
        }
        catch(IOException e){

        }

    }
    public static void sendScript(String script){
        StringBuffer instr = new StringBuffer();
        try{
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
            out.println(script);
            out.flush();
            InputStreamReader isr = new InputStreamReader(bis, "US-ASCII");
            int c;
            while((c = isr.read()) != 10){
                System.out.print((char)c);
            }
            System.out.println();


        }
        catch(Exception e){
            System.err.println(e);
        }
    }
    public static void closeConnectionToRobot(){
        try{
            socket.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }


    public static List<String> comandes(){
        List<String> listComandes = asList(
                "load <program.urp>",
                "play",
                "stop",
                "pause",
                "quit",
                "shutdown",
                "running",
                "robotmode",
                "get loaded program",
                "popup <popup-text>",
                "close popup",
                "addToLog <log-message>",
                "isProgramSaved",
                "programState",
                "PolyscopeVersion",
                "setUserRole <programmer/operator/none/locked>",
                "setUserRole <role> restricted",
                "getUserRole",
                "power on",
                "power off",
                "brake release",
                "safetymode",
                "unlock protective stop",
                "close safety stop",
                "load installation <default.installation>",
                "restart safety",
                "get serial number",
                "get robot model",
                "generate flight report <controller/software/system>",
                "generate support file <Directory path>"
        );

        return listComandes;
    }

    public static void printCommonNames(Collection<String> commons, int width, int columnCount) {
        int column = 0;
        String format = "%-" + width + "s";
        for (String name : commons) {
            System.out.printf(format, name);
            column = ++column % columnCount;
            if (column == 0) System.out.println();
        }
    }
}