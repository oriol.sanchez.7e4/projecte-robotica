import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        SocketService.connectToRobot();

        System.out.println("==============================================================================================================================================================================================");
        System.out.println("                                                                                         ALL COMMANDS                                                                                         ");
        System.out.println("==============================================================================================================================================================================================");
        List<String> comandes = SocketService.comandes();

        SocketService.printCommonNames(comandes, 70, 3);
        System.out.println("==============================================================================================================================================================================================");

        Scanner sc = new Scanner(System.in).useLocale(Locale.US);
        String str = sc.nextLine();
        while (!str.equals("END")) {
            SocketService.sendScript(str);
            str = sc.nextLine();
        }

        SocketService.closeConnectionToRobot();
    }
}
